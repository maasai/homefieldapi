<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//, 'middleware'=>'auth:api'
Route::group(array('prefix' => '/v1', 'middleware' => ['jsonify']), function () {

    Route::post('/logout', 'Api\Auth\LoginController@logout');

    Route::resource('users', 'Api\UserController', ['except' => ['create', 'edit']]);
    Route::post('users/search', array('as' => 'users.search', 'uses' => 'Api\UserController@search'));

    Route::get('/me', 'Api\UserController@me');

    Route::resource('permissions', 'Api\PermissionController', ['except' => ['create', 'edit']]);
    Route::post('permissions/search', array('as' => 'permissions.search', 'uses' => 'Api\PermissionController@search'));

    Route::resource('roles', 'Api\RoleController');
    Route::post('roles/search', array('as' => 'roles.search', 'uses' => 'Api\RoleController@search'));

    Route::resource('apartments', 'Api\ApartmentController');
    Route::post('apartments/search', array('as' => 'apartments.search', 'uses' => 'Api\ApartmentController@search'));

    Route::resource('tenants', 'Api\TenantController');
    Route::post('tenants/search', array('as' => 'tenants.search', 'uses' => 'Api\TenantController@search'));

    Route::resource('care_takers', 'Api\CareTakerController');
    Route::post('care_takers/search', array('as' => 'care_takers.search', 'uses' => 'Api\CareTakerController@search'));

    Route::resource('payments', 'Api\PaymentController');
    Route::post('payments/search', array('as' => 'payments.search', 'uses' => 'Api\PaymentController@search'));

    Route::resource('invoices', 'Api\InvoiceController');
    Route::post('invoices/search', array('as' => 'invoices.search', 'uses' => 'Api\InvoiceController@search'));

    Route::resource('expenses', 'Api\ExpenseController');
    Route::post('expenses/search', array('as' => 'expenses.search', 'uses' => 'Api\ExpenseController@search'));

    Route::resource('receipts', 'Api\ReceiptController');
    Route::post('receipts/search', array('as' => 'receipts.search', 'uses' => 'Api\ReceiptController@search'));

    Route::resource('payment_methods', 'Api\PaymentMethodController');
    Route::post('payment_methods/search', array('as' => 'payment_methods.search', 'uses' => 'Api\PaymentMethodController@search'));

    Route::resource('invoice_statuses', 'Api\InvoiceStatusController');
    Route::post('invoice_statuses/search', array('as' => 'invoice_statuses.search', 'uses' => 'Api\InvoiceStatusController@search'));

    Route::resource('payment_categories', 'Api\PaymentCategoryController');
    Route::post('payment_categories/search', array('as' => 'payment_categories.search', 'uses' => 'Api\PaymentCategoryController@search'));

    Route::resource('leases', 'Api\LeaseController');
    Route::post('leases/search', array('as' => 'leases.search', 'uses' => 'Api\LeaseController@search'));

    Route::resource('house_units', 'Api\HouseUnitController');
    Route::post('house_units/search', array('as' => 'house_units.search', 'uses' => 'Api\HouseUnitController@search'));

    Route::resource('leads', 'Api\LeadController');
    Route::post('leads/search', array('as' => 'leads.search', 'uses' => 'Api\LeadController@search'));

    Route::resource('house_statuses', 'Api\HouseStatusController');
    Route::post('house_statuses/search', array('as' => 'house_statuses.search', 'uses' => 'Api\HouseStatusController@search'));


});