<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('uuid', 36)->primary()->unique();
            $table->string('user_id', 36);

            $table->string('nationality');
            $table->string('id_number');
            $table->string('passport_number')->nullable();
            $table->string('postal_address')->nullable();

            $table->string('marital_status')->nullable();
            $table->string('number_of_intended_occupants')->nullable();
            $table->string('occupant_relationship')->nullable();

            $table->string('emergency_full_name')->nullable();
            $table->string('emergency_postal_address')->nullable();
            $table->string('emergency_phone')->nullable();
            $table->string('emergency_email')->nullable();

            $table->string('employment_status')->nullable();
            $table->string('employment_employer')->nullable();
            $table->string('employment_position_held')->nullable();
            $table->string('employment_physical_address')->nullable();
            $table->string('employment_phone')->nullable();

            $table->string('self_employed_business')->nullable();
            $table->string('self_employed_business_address')->nullable();

            $table->string('self_rent_payer')->nullable();
            $table->string('rent_payer')->nullable();
            $table->string('rent_payer_contact')->nullable();

            $table->string('date_joined')->nullable();
            $table->string('commencement_of_lease')->nullable();

            $table->string('kra_pin')->nullable();
            $table->string('passport_photo_url')->nullable();

            $table->rememberToken();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
