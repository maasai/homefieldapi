<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareTakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('care_takers', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('uuid', 36)->primary()->unique();

            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique();

            $table->string('nationality');
            $table->string('id_number');
            $table->string('kra_pin')->nullable();
            $table->string('passport_number')->nullable();
            $table->string('postal_address')->nullable();
            $table->string('marital_status')->nullable();

            $table->string('emergency_full_name')->nullable();
            $table->string('emergency_postal_address')->nullable();
            $table->string('emergency_phone')->nullable();
            $table->string('emergency_email')->nullable();

            $table->string('first_referee_full_name')->nullable();
            $table->string('first_referee_phone')->nullable();
            $table->string('first_referee_address')->nullable();
            $table->string('first_referee_email')->nullable();

            $table->string('second_referee_full_name')->nullable();
            $table->string('second_referee_phone')->nullable();
            $table->string('second_referee_address')->nullable();
            $table->string('second_referee_email')->nullable();

            $table->string('third_referee_full_name')->nullable();
            $table->string('third_referee_phone')->nullable();
            $table->string('third_referee_address')->nullable();
            $table->string('third_referee_email')->nullable();

            $table->rememberToken();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('care_takers');
    }
}
