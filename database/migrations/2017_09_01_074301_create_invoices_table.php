<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('uuid', 36)->primary()->unique();

            $table->string('invoice_number');
            $table->string('invoice_status_id');
            $table->string('lease_id');
            $table->string('tenant_id',36);
            $table->date('invoice_date');
            $table->date('due_date');
            $table->float('amount_total');
            $table->float('amount_paid');
            $table->float('amount_due');
            $table->float('amount');
            $table->float('received_amount');
            $table->date('paid_date');
            $table->string('notes');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
