<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('uuid', 36)->primary()->unique();

            $table->string('lease_id', 36)->nullable();
            $table->string('tenant_id', 36)->nullable();
            $table->string('invoice_id', 36);
            $table->string('staff_id', 36);

            $table->string('paid_by');
            $table->string('reference_number')->unique();
            $table->string('payment_date');
            $table->string('amount');
            $table->string('payment_method_id', 36);
            $table->string('payment_category_id', 36);
            $table->string('other_details')->nullable()->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
