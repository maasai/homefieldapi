<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_units', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('uuid', 36)->primary()->unique();

            $table->string('apartment_id', 36);
            $table->string('house_number')->unique();
            $table->string('bedroom_count')->nullable();
            $table->string('bathroom_count')->nullable();
            $table->string('garage_attached')->nullable();
            $table->string('area_sq_feet')->nullable();
            $table->string('unit_status')->nullable();
            $table->string('parking_available')->nullable();
            $table->string('balcony_available')->nullable();
            $table->string('floor_type')->nullable();
            $table->string('commercial')->nullable();
            $table->string('notes')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_units');
    }
}
