<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('uuid', 36)->primary()->unique();

            $table->string('landlord_id', 36);
            $table->string('operations_officer_incharge_id', 36)->nullable(); // A staff
            $table->string('care_taker_id', 36)->nullable();

            $table->string('apartment_name')->unique();
            $table->string('apartment_reference_number')->unique();
            $table->string('address')->nullable();
            $table->string('geo')->nullable();

            $table->string('smoking_allowed')->nullable();
            $table->string('pets_allowed')->nullable();

            $table->string('total_single_units')->nullable();
            $table->string('total_bedsitters')->nullable();
            $table->string('total_one_bedrooms')->nullable();
            $table->string('total_two_bedrooms')->nullable();
            $table->string('total_three_bedrooms')->nullable();
            $table->string('total_four_bedrooms')->nullable();
            $table->string('total_commercial_rooms')->nullable();

            $table->string('extra_details')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
