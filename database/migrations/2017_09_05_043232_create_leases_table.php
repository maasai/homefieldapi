<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leases', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('uuid', 36)->primary()->unique();

            $table->string('tenant_id', 36);
            $table->string('apartment_id', 36);
            $table->string('house_unit_id', 36);

            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('termination_date')->nullable();
            $table->string('termination_notes')->nullable();

            $table->string('monthly_rent')->nullable();
            $table->string('rent_security_deposit')->nullable();
            $table->string('rent_due_date')->nullable();

            $table->string('pet_deposit')->nullable();
            $table->string('electricity_deposit')->nullable();
            $table->string('water_deposit')->nullable();
            $table->string('total_other_deposits')->nullable();
            $table->string('other_deposits_description')->nullable();

            $table->string('lease_status')->nullable();
            $table->string('apply_fine_on_late_payment')->nullable();

            $table->string('served_by_staff_id')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leases');
    }
}
