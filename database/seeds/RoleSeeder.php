<?php

namespace database\seeds;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{

    public function run()
    {

        DB::table('roles')->delete();

        $role = new Role();

        $userPermissions = Permission::select('uuid')
            ->whereIn('permission_name', ['manage-permissions', 'manage-roles', 'manage-users', 'manage-raw_texts', 'manage-recordings', 'manage-recording_transcriptions'])
            ->pluck('uuid');


        $transcriberPermissions = Permission::select('uuid')
            ->whereIn('permission_name', ['manage-recording_transcriptions'])
            ->pluck('uuid');

        //Create role 'User' and assign permissions
        $role->create([
            'role_name' => 'Admin',
            'role_display_name' => 'Admin',
            'role_description' => 'Admin'
        ])->permissions()->attach($userPermissions);

        //Create 'Transcriber' role and assign necessary permissions
        $role->create([
            'role_name' => 'Staff',
            'role_display_name' => 'Staff',
            'role_description' => 'Staff'
        ])->permissions()->attach($transcriberPermissions);


        $role->create([
            'role_name' => 'Landlord',
            'role_display_name' => 'Landlord',
            'role_description' => 'Landlord'
        ])->permissions()->attach($transcriberPermissions);

        $role->create([
            'role_name' => 'Tenant',
            'role_display_name' => 'Tenant',
            'role_description' => 'Tenant'
        ])->permissions()->attach($transcriberPermissions);
    }

}