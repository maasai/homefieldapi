<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HomeFieldServiceProvider extends ServiceProvider {

    /**
     * System repositories
     * @var array
     */
    protected $repositories = [
        'Apartment',
        'CareTaker',
        'User',
        'Role',
        'Permission',
        'Tenant',
        'Payment',
        'Expense',
        'Invoice',
        'Receipt',
        'PaymentMethod',
        'InvoiceStatus',
        'PaymentCategory',
        'Lease',
        'HouseUnit',
        'Lead',
        'HouseStatus',
        'ApartmentPaymentOption'
    ];

    /**
     *  Loops through all repositories and binds them with their Eloquent implementation
     */
    public function register()
    {
        array_walk($this->repositories, function($repository) {
            $this->app->bind(
                'App\HomeField\Repositories\Contracts\\'. $repository . 'Interface',
                'App\HomeField\Repositories\Eloquent\\' . $repository . 'Repository'
            );
        });

    }



}