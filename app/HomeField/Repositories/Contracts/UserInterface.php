<?php

namespace App\HomeField\Repositories\Contracts;

interface UserInterface extends BaseInterface {

    /**
     * @param $user
     * @return mixed
     */
    function isSolicitor($user);

}