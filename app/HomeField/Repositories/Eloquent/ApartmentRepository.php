<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Apartment;
use App\HomeField\Repositories\Contracts\ApartmentInterface;

class ApartmentRepository extends BaseRepository implements ApartmentInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * ApartmentRepository constructor.
     * @param Apartment $model
     */
    function __construct(Apartment $model)
    {
        $this->model = $model;
    }

}