<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Receipt;
use App\HomeField\Repositories\Contracts\ReceiptInterface;

class ReceiptRepository extends BaseRepository implements ReceiptInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * ReceiptRepository constructor.
     * @param Receipt $model
     */
    function __construct(Receipt $model)
    {
        $this->model = $model;
    }

}