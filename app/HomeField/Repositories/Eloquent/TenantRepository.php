<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Tenant;
use App\HomeField\Repositories\Contracts\TenantInterface;

class TenantRepository extends BaseRepository implements TenantInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * TenantRepository constructor.
     * @param Tenant $model
     */
    function __construct(Tenant $model)
    {
        $this->model = $model;
    }

}