<?php


namespace App\HomeField\Repositories\Eloquent;

use App\HomeField\Transformers\RoleTransformer;
use App\Models\Role;
use App\HomeField\Repositories\Contracts\RoleInterface;

class RoleRepository extends BaseRepository implements RoleInterface {

    protected $model, $transformer;

    /**
     * RoleRepository constructor.
     * @param Role $model
     * @param RoleTransformer $transformer
     */
    function __construct(Role $model, RoleTransformer $transformer)
    {
        $this->model = $model;
        $this->transformer = $transformer;

    }

}