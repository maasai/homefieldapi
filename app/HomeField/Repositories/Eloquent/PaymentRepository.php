<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Payment;
use App\HomeField\Repositories\Contracts\PaymentInterface;

class PaymentRepository extends BaseRepository implements PaymentInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * PaymentRepository constructor.
     * @param Payment $model
     */
    function __construct(Payment $model)
    {
        $this->model = $model;
    }

}