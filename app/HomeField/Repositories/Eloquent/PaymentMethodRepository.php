<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\PaymentMethod;
use App\HomeField\Repositories\Contracts\PaymentMethodInterface;

class PaymentMethodRepository extends BaseRepository implements PaymentMethodInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * PaymentRepository constructor.
     * @param PaymentMethod $model
     */
    function __construct(PaymentMethod $model)
    {
        $this->model = $model;
    }

}