<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\HouseStatus;
use App\HomeField\Repositories\Contracts\HouseStatusInterface;

class HouseStatusRepository extends BaseRepository implements HouseStatusInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * HouseStatusRepository constructor.
     * @param HouseStatus $model
     */
    function __construct(HouseStatus $model)
    {
        $this->model = $model;
    }

}