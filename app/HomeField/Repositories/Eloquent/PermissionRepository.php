<?php

namespace App\HomeField\Repositories\Eloquent;

use App\HomeField\Transformers\PermissionTransformer;
use App\Models\Permission;
use App\HomeField\Repositories\Contracts\PermissionInterface;

class PermissionRepository extends BaseRepository implements PermissionInterface {

    protected $model, $transformer;

    /**
     * PermissionRepository constructor.
     * @param Permission $model
     * @param PermissionTransformer $transformer
     */
    function __construct(Permission $model, PermissionTransformer $transformer)
    {
        $this->model = $model;
        $this->transformer = $transformer;
    }

}