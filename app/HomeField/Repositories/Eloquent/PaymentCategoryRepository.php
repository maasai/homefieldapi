<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\PaymentCategory;
use App\HomeField\Repositories\Contracts\PaymentCategoryInterface;

class PaymentCategoryRepository extends BaseRepository implements PaymentCategoryInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * PaymentCategoryRepository constructor.
     * @param PaymentCategory $model
     */
    function __construct(PaymentCategory $model)
    {
        $this->model = $model;
    }

}