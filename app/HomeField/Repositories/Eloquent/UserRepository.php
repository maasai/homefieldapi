<?php


namespace App\HomeField\Repositories\Eloquent;

use App\HomeField\Repositories\Contracts\RoleInterface;
use App\HomeField\Transformers\UserTransformer;
use App\Models\User;
use App\HomeField\Repositories\Contracts\UserInterface;

class UserRepository extends BaseRepository implements UserInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * UserRepository constructor.
     * @param User $model
     * @param UserTransformer $transformer
     * @param RoleInterface $roleRepository
     */
    function __construct(User $model, UserTransformer $transformer, RoleInterface $roleRepository)
    {
        $this->model = $model;
        $this->transformer = $transformer;
        $this->roleRepository = $roleRepository;
    }


    /**
     * @param $user
     * @return bool
     */
    public function isSolicitor($user)
    {
        if(strtolower($this->roleRepository->getById($user->role_id)->role_name) === "solicitor"){
            return true;
        }
        return false;

    }

}