<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\InvoiceStatus;
use App\HomeField\Repositories\Contracts\InvoiceStatusInterface;

class InvoiceStatusRepository extends BaseRepository implements InvoiceStatusInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * InvoiceStatusRepository constructor.
     * @param InvoiceStatus $model
     */
    function __construct(InvoiceStatus $model)
    {
        $this->model = $model;
    }

}