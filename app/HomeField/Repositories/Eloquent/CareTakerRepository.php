<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\CareTaker;
use App\HomeField\Repositories\Contracts\CareTakerInterface;

class CareTakerRepository extends BaseRepository implements CareTakerInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * CareTakerRepository constructor.
     * @param CareTaker $model
     */
    function __construct(CareTaker $model)
    {
        $this->model = $model;
    }

}