<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Lead;
use App\HomeField\Repositories\Contracts\LeadInterface;

class LeadRepository extends BaseRepository implements LeadInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * LeadRepository constructor.
     * @param Lead $model
     */
    function __construct(Lead $model)
    {
        $this->model = $model;
    }

}