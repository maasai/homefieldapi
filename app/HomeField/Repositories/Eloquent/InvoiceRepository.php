<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Invoice;
use App\HomeField\Repositories\Contracts\InvoiceInterface;

class InvoiceRepository extends BaseRepository implements InvoiceInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * InvoiceRepository constructor.
     * @param Invoice $model
     */
    function __construct(Invoice $model)
    {
        $this->model = $model;
    }

}