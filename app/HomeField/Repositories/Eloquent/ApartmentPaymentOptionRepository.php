<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\ApartmentPaymentOption;
use App\HomeField\Repositories\Contracts\ApartmentPaymentOptionInterface;

class ApartmentPaymentOptionRepository extends BaseRepository implements ApartmentPaymentOptionInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * ApartmentPaymentOptionRepository constructor.
     * @param ApartmentPaymentOption $model
     */
    function __construct(ApartmentPaymentOption $model)
    {
        $this->model = $model;
    }

}