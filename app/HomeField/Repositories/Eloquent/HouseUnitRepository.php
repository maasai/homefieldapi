<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\HouseUnit;
use App\HomeField\Repositories\Contracts\HouseUnitInterface;

class HouseUnitRepository extends BaseRepository implements HouseUnitInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * HouseUnitRepository constructor.
     * @param HouseUnit $model
     */
    function __construct(HouseUnit $model)
    {
        $this->model = $model;
    }

}