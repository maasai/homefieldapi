<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Expense;
use App\HomeField\Repositories\Contracts\ExpenseInterface;

class ExpenseRepository extends BaseRepository implements ExpenseInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * ExpenseRepository constructor.
     * @param Expense $model
     */
    function __construct(Expense $model)
    {
        $this->model = $model;
    }

}