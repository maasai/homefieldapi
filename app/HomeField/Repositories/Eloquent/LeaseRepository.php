<?php

namespace App\HomeField\Repositories\Eloquent;

use App\Models\Lease;
use App\HomeField\Repositories\Contracts\LeaseInterface;

class LeaseRepository extends BaseRepository implements LeaseInterface {

    protected $model, $transformer, $roleRepository;

    /**
     * LeaseRepository constructor.
     * @param Lease $model
     */
    function __construct(Lease $model)
    {
        $this->model = $model;
    }

}