<?php

namespace App\HomeField\Transformers;

class PaymentTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                    => $data['uuid'],
            'lease_id'              => $data['lease_id'],
            'tenant_id'             => $data['tenant_id'],
            'invoice_id'            => $data['invoice_id'],
            'paid_by'               => $data['paid_by'],
            'reference_number'      => $data['reference_number'],
            'payment_date'          => $data['payment_date'],
            'amount'                => $data['amount'],
            'payment_method_id'     => $data['payment_method_id'],
            'payment_category_id'   => $data['payment_category_id'],
            'other_details'         => $data['other_details'],
            'created_at'            => $this->dateTransformer->transform($data['created_at']),
            'updated_at'            => $this->dateTransformer->transform($data['updated_at'])
        ];

        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                    => 'uuid',
            'lease_id'              => 'lease_id',
            'tenant_id'             => 'tenant_id',
            'invoice_id'            => 'invoice_id',
            'paid_by'               => 'paid_by',
            'reference_number'      => 'reference_number',
            'payment_date'          => 'payment_date',
            'amount'                => 'amount',
            'payment_method_id'     => 'payment_method_id',
            'payment_category_id'   => 'payment_category_id',
            'other_details'         => 'other_details',
            'created_at'            => 'created_at',
            'updated_at'            => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}