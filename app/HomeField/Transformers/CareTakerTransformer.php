<?php

namespace App\HomeField\Transformers;

class CareTakerTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                        => $data['uuid'],
            'first_name'                => $data['first_name'],
            'last_name'                 => $data['last_name'],
            'email'                     => $data['email'],
            'phone'                     => $data['phone'],
            'nationality'               => $data['nationality'],
            'id_number'                 => $data['id_number'],
            'kra_pin'                   => $data['kra_pin'],
            'passport_number'           => $data['passport_number'],
            'postal_address'            => $data['postal_address'],
            'marital_status'            => $data['marital_status'],
            'emergency_full_name'       => $data['emergency_full_name'],
            'emergency_postal_address'  => $data['emergency_postal_address'],
            'emergency_phone'           => $data['emergency_phone'],
            'emergency_email'           => $data['emergency_email'],
            'first_referee_full_name'   => $data['first_referee_full_name'],
            'first_referee_phone'       => $data['first_referee_phone'],
            'first_referee_address'     => $data['first_referee_address'],
            'first_referee_email'       => $data['first_referee_email'],
            'second_referee_full_name'  => $data['second_referee_full_name'],
            'second_referee_phone'      => $data['second_referee_phone'],
            'second_referee_address'    => $data['second_referee_address'],
            'second_referee_email'      => $data['second_referee_email'],
            'third_referee_full_name'   => $data['third_referee_full_name'],
            'third_referee_phone'       => $data['third_referee_phone'],
            'third_referee_address'     => $data['third_referee_address'],
            'third_referee_email'       => $data['third_referee_email'],
            'created_at'                => $this->dateTransformer->transform($data['created_at']),
            'updated_at'                => $this->dateTransformer->transform($data['updated_at'])

        ];


        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                        => 'uuid',
            'first_name'                => 'first_name',
            'last_name'                 => 'last_name',
            'email'                     => 'email',
            'phone'                     => 'phone',
            'nationality'               => 'nationality',
            'id_number'                 => 'id_number',
            'kra_pin'                   => 'kra_pin',
            'passport_number'           => 'passport_number',
            'postal_address'            => 'postal_address',
            'marital_status'            => 'marital_status',
            'emergency_full_name'       => 'emergency_full_name',
            'emergency_postal_address'  => 'emergency_postal_address',
            'emergency_phone'           => 'emergency_phone',
            'emergency_email'           => 'emergency_email',
            'first_referee_full_name'   => 'first_referee_full_name',
            'first_referee_phone'       => 'first_referee_phone',
            'first_referee_address'     => 'first_referee_address',
            'first_referee_email'       => 'first_referee_email',
            'second_referee_full_name'  => 'second_referee_full_name',
            'second_referee_phone'      => 'second_referee_phone',
            'second_referee_address'    => 'second_referee_address',
            'second_referee_email'      => 'second_referee_email',
            'third_referee_full_name'   => 'third_referee_full_name',
            'third_referee_phone'       => 'third_referee_phone',
            'third_referee_address'     => 'third_referee_address',
            'third_referee_email'       => 'third_referee_email',
            'created_at'                => 'created_at',
            'updated_at'                => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}