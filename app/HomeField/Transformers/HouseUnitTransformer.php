<?php

namespace App\HomeField\Transformers;

class HouseUnitTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                    => $data['uuid'],
            'apartment_id'          => $data['apartment_id'],
            'house_number'          => $data['house_number'],
            'bedroom_count'         => $data['bedroom_count'],
            'bathroom_count'        => $data['bathroom_count'],
            'garage_attached'       => $data['garage_attached'],
            'area_sq_feet'          => $data['area_sq_feet'],
            'unit_status'           => $data['unit_status'],
            'commercial'            => $data['commercial'],
            'parking_available'     => $data['parking_available'],
            'balcony_available'     => $data['balcony_available'],
            'floor_type'            => $data['floor_type'],
            'notes'                 => $data['notes'],
            'created_at'            => $this->dateTransformer->transform($data['created_at']),
            'updated_at'            => $this->dateTransformer->transform($data['updated_at'])

        ];


        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                    => 'uuid',
            'apartment_id'          => 'apartment_id',
            'house_number'          => 'house_number',
            'bedroom_count'         => 'bedroom_count',
            'bathroom_count'        => 'bathroom_count',
            'garage_attached'       => 'garage_attached',
            'area_sq_feet'          => 'area_sq_feet',
            'unit_status'           => 'unit_status',
            'commercial'            => 'commercial',
            'parking_available'     => 'parking_available',
            'balcony_available'     => 'balcony_available',
            'floor_type'            => 'floor_type',
            'notes'                 => 'notes',
            'created_at'            => 'created_at',
            'updated_at'            => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}