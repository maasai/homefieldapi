<?php

namespace App\HomeField\Transformers;

/**
 * Class BaseTransformer
 * @package App\Sproose\Transformers
 */
abstract class BaseTransformer {

    /**
     * Implement to hide the db table field names
     * @param $item
     * @return mixed
     */
    public abstract function transform($item);

    /**
     * Transform a collection of provided items
     * @param array $items
     * @return array
     */
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }


    /** Convert fields from what is exposed back to what is on database table
     * @param $field
     * @param $entityFields
     * @return bool
     */
    public function reverseField($field, $entityFields)
    {
        if (array_key_exists($field, $entityFields)) {
            return $entityFields[$field];
        }else

            return false;
    }

}