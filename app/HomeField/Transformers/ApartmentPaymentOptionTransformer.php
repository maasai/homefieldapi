<?php

namespace App\HomeField\Transformers;

class ApartmentPaymentOptionTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                        => $data['uuid'],
            'apartment_id'              => $data['apartment_id'],
            'payment_option_details'    => $data['payment_option_details'],
            'created_at'                => $this->dateTransformer->transform($data['created_at']),
            'updated_at'                => $this->dateTransformer->transform($data['updated_at'])
        ];

        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                        => 'uuid',
            'apartment_id'              => 'apartment_id',
            'payment_option_details'    => 'payment_option_details',
            'created_at'                => 'created_at',
            'updated_at'                => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}