<?php

namespace App\HomeField\Transformers;

class LeadTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                    => $data['uuid'],
            'full_name'             => $data['full_name'],
            'phone'                 => $data['phone'],
            'service_needed'        => $data['service_needed'],
            'address'               => $data['address'],
            'budget'                => $data['budget'],
            'gender'                => $data['gender'],
            'created_at'            => $this->dateTransformer->transform($data['created_at']),
            'updated_at'            => $this->dateTransformer->transform($data['updated_at'])

        ];


        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                => 'uuid',
            'full_name'         => 'full_name',
            'phone'             => 'phone',
            'service_needed'    => 'service_needed',
            'address'           => 'address',
            'budget'            => 'budget',
            'gender'            => 'gender',
            'created_at'            => 'created_at',
            'updated_at'            => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}