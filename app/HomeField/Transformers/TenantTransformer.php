<?php

namespace App\HomeField\Transformers;

class TenantTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                                => $data['uuid'],
            'user_id'                           => $data['user_id'],
            'nationality'                       => $data['nationality'],
            'id_number'                         => $data['id_number'],
            'passport_number'                   => $data['passport_number'],
            'postal_address'                    => $data['postal_address'],
            'marital_status'                    => $data['marital_status'],
            'number_of_intended_occupants'      => $data['number_of_intended_occupants'],
            'occupant_relationship'             => $data['occupant_relationship'],
            'emergency_full_name'               => $data['emergency_full_name'],
            'emergency_postal_address'          => $data['emergency_postal_address'],
            'emergency_phone'                   => $data['emergency_phone'],
            'emergency_email'                   => $data['emergency_email'],
            'employment_status'                 => $data['employment_status'],
            'employment_employer'               => $data['employment_employer'],
            'employment_position_held'          => $data['employment_position_held'],
            'employment_physical_address'       => $data['employment_physical_address'],
            'employment_phone'                  => $data['employment_phone'],
            'self_employed_business'            => $data['self_employed_business'],
            'self_employed_business_address'    => $data['self_employed_business_address'],
            'self_rent_payer'                   => $data['self_rent_payer'],
            'rent_payer'                        => $data['rent_payer'],
            'rent_payer_contact'                => $data['rent_payer_contact'],
            'date_joined'                       => $data['date_joined'],
            'commencement_of_lease'             => $data['commencement_of_lease'],
            'kra_pin'                           => $data['kra_pin'],
            'passport_photo_url'                => $data['passport_photo_url'],
            'created_at'                        => $this->dateTransformer->transform($data['created_at']),
            'updated_at'                        => $this->dateTransformer->transform($data['updated_at'])
        ];


        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                                => 'uuid',
            'user_id'                           => 'user_id',
            'nationality'                       => 'nationality',
            'id_number'                         => 'id_number',
            'passport_number'                   => 'passport_number',
            'postal_address'                    => 'postal_address',
            'marital_status'                    => 'marital_status',
            'number_of_intended_occupants'      => 'number_of_intended_occupants',
            'occupant_relationship'             => 'occupant_relationship',
            'emergency_full_name'               => 'emergency_full_name',
            'emergency_postal_address'          => 'emergency_postal_address',
            'emergency_phone'                   => 'emergency_phone',
            'emergency_email'                   => 'emergency_email',
            'employment_status'                 => 'employment_status',
            'employment_employer'               => 'employment_employer',
            'employment_position_held'          => 'employment_position_held',
            'employment_physical_address'       => 'employment_physical_address',
            'employment_phone'                  => 'employment_phone',
            'self_employed_business'            => 'self_employed_business',
            'self_employed_business_address'    => 'self_employed_business_address',
            'self_rent_payer'                   => 'self_rent_payer',
            'rent_payer'                        => 'rent_payer',
            'rent_payer_contact'                => 'rent_payer_contact',
            'date_joined'                       => 'date_joined',
            'commencement_of_lease'             => 'commencement_of_lease',
            'kra_pin'                           => 'kra_pin',
            'passport_photo_url'                => 'passport_photo_url',
            'created_at'                        => 'created_at',
            'updated_at'                        => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}