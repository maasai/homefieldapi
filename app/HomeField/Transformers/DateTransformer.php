<?php


namespace App\HomeField\Transformers;

class DateTransformer extends BaseTransformer
{

    public function transform($date)
    {

        return ($date == null) ? null : [
            'timezone'    => $date->timezoneName,
            'value'       => $date->toDateTimeString(),
            'pretty_date' => date('d-M-Y', strtotime($date->toDateTimeString())),
            'pretty_time' => date('H:i', strtotime($date->toDateTimeString()))
        ];
    }

}