<?php

namespace App\HomeField\Transformers;

class PermissionTransformer extends BaseTransformer {

    private $dateTransformer;

    /**
     * BookingDurationTransformer constructor.
     * @param DateTransformer $dateTransformer
     */
    function __construct(DateTransformer $dateTransformer){

        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                        => $data['uuid'],
            'permission_name'           => $data['permission_name'],
            'permission_display_name'   => $data['permission_display_name'],
            'permission_description'    => $data['permission_description'],
            'created_at'                => $this->dateTransformer->transform($data['created_at']),
            'updated_at'                => $this->dateTransformer->transform($data['updated_at'])
        ];


        return $allFields;
    }

    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                        => 'uuid',
            'permission_name'           => 'permission_name',
            'permission_display_name'   => 'permission_display_name',
            'permission_description'    => 'permission_description',
            'created_at'                => 'created_at',
            'updated_at'                => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}