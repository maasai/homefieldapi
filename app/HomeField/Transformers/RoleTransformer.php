<?php


namespace App\HomeField\Transformers;

class RoleTransformer extends BaseTransformer {

    private $dateTransformer;

    /**
     * BookingDurationTransformer constructor.
     * @param DateTransformer $dateTransformer
     */
    function __construct(DateTransformer $dateTransformer){

        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                => $data['uuid'],
            'role_name'         => $data['role_name'],
            'role_display_name' => $data['role_display_name'],
            'role_description'  => $data['role_description'],
            'permissions'       => $data['permissions'],
            'created_at'        => $this->dateTransformer->transform($data['created_at']),
            'updated_at'        => $this->dateTransformer->transform($data['updated_at'])
        ];


        return $allFields;
    }

    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                => 'uuid',
            'role_name'         => 'role_name',
            'role_display_name' => 'role_display_name',
            'role_description'  => 'role_description',
            'created_at'        => 'created_at',
            'updated_at'        => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}