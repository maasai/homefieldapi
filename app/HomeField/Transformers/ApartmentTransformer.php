<?php

namespace App\HomeField\Transformers;

class ApartmentTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                                => $data['uuid'],
            'landlord_id'                       => $data['landlord_id'],
            'operations_officer_incharge_id'    => $data['operations_officer_incharge_id'],
            'apartment_name'                    => $data['apartment_name'],
            'apartment_reference_number'        => $data['apartment_reference_number'],
            'address'                           => $data['address'],
            'geo'                               => $data['geo'],
            'smoking_allowed'                   => $data['smoking_allowed'],
            'pets_allowed'                      => $data['pets_allowed'],
            'total_single_units'                => $data['total_single_units'],
            'total_bedsitters'                  => $data['total_bedsitters'],
            'total_one_bedrooms'                => $data['total_one_bedrooms'],
            'total_two_bedrooms'                => $data['total_two_bedrooms'],
            'total_three_bedrooms'              => $data['total_three_bedrooms'],
            'total_four_bedrooms'               => $data['total_four_bedrooms'],
            'total_commercial_rooms'            => $data['total_commercial_rooms'],
            'extra_details'                     => $data['extra_details'],
            'created_at'                        => $this->dateTransformer->transform($data['created_at']),
            'updated_at'                        => $this->dateTransformer->transform($data['updated_at'])
        ];

        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                                => 'uuid',
            'landlord_id'                       => 'landlord_id',
            'operations_officer_incharge_id'    => 'operations_officer_incharge_id',
            'apartment_name'                    => 'apartment_name',
            'apartment_reference_number'        => 'apartment_reference_number',
            'address'                           => 'address',
            'geo'                               => 'geo',
            'smoking_allowed'                   => 'smoking_allowed',
            'pets_allowed'                      => 'pets_allowed',
            'total_single_units'                => 'total_single_units',
            'total_bedsitters'                  => 'total_bedsitters',
            'total_one_bedrooms'                => 'total_one_bedrooms',
            'total_two_bedrooms'                => 'total_two_bedrooms',
            'total_three_bedrooms'              => 'total_three_bedrooms',
            'total_four_bedrooms'               => 'total_four_bedrooms',
            'total_commercial_rooms'            => 'total_commercial_rooms',
            'extra_details'                     => 'extra_details',
            'created_at'                        => 'created_at',
            'updated_at'                        => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}