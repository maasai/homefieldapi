<?php

namespace App\HomeField\Transformers;

class UserTransformer extends BaseTransformer {

    private $roleTransformer, $dateTransformer;

    function __construct(RoleTransformer $roleTransformer, DateTransformer $dateTransformer){

        $this->roleTransformer = $roleTransformer;
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                    => $data['uuid'],
            'first_name'            => $data['first_name'],
            'last_name'             => $data['last_name'],
            'email'                 => $data['email'],
            'salutation'            => $data['salutation'],
            'phone'                 => $data['phone'],
            'address'               => $data['address'],
            'profile_picture_url'   => $data['profile_picture_url'],
            'role_id'               => $data['role_id'],
            'total_properties'      => count($data['properties']),
            'total_bookings'        => count($data['bookings']),
            'bookings'              => $data['bookings'],
            'role'                  => $data['role'],
            'created_at'            => $this->dateTransformer->transform($data['created_at']),
            'updated_at'            => $this->dateTransformer->transform($data['updated_at'])

        ];


        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                    => 'uuid',
            'first_name'            => 'first_name',
            'last_name'             => 'last_name',
            'email'                 => 'email',
            'salutation'            => 'salutation',
            'phone'                 => 'phone',
            'address'               => 'address',
            'profile_picture_url'   => 'profile_picture_url',
            'role_id'               => 'role_id',

            'role'                  => 'role',

            'created_at'            => 'created_at',
            'updated_at'            => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}