<?php

namespace App\HomeField\Transformers;

class LeaseTransformer extends BaseTransformer {

    private $dateTransformer;

    function __construct(DateTransformer $dateTransformer){
        $this->dateTransformer = $dateTransformer;
    }

    /**
     * @param $data
     * @return array|mixed
     */
    public function transform($data)
    {
        $allFields = [
            'id'                            => $data['uuid'],
            'tenant_id'                     => $data['tenant_id'],
            'apartment_id'                  => $data['apartment_id'],
            'house_unit_id'                 => $data['house_unit_id'],
            'start_date'                    => $data['start_date'],
            'end_date'                      => $data['end_date'],
            'termination_date'              => $data['termination_date'],
            'termination_notes'             => $data['termination_notes'],
            'monthly_rent'                  => $data['monthly_rent'],
            'rent_security_deposit'         => $data['rent_security_deposit'],
            'rent_due_date'                 => $data['rent_due_date'],
            'pet_deposit'                   => $data['pet_deposit'],
            'electricity_deposit'           => $data['electricity_deposit'],
            'water_deposit'                 => $data['water_deposit'],
            'total_other_deposits'          => $data['total_other_deposits'],
            'other_deposits_description'    => $data['other_deposits_description'],
            'lease_status'                  => $data['lease_status'],
            'apply_fine_on_late_payment'    => $data['apply_fine_on_late_payment'],
            'served_by_staff_id'            => $data['served_by_staff_id'],
            'created_at'                    => $this->dateTransformer->transform($data['created_at']),
            'updated_at'                    => $this->dateTransformer->transform($data['updated_at'])
        ];

        return $allFields;
    }


    /**
     * Original table field names with associated transformed names
     * @return array
     */
    public function fieldMap(){
        return [
            'id'                            => 'uuid',
            'tenant_id'                     => 'tenant_id',
            'apartment_id'                  => 'apartment_id',
            'house_unit_id'                 => 'house_unit_id',
            'start_date'                    => 'start_date',
            'end_date'                      => 'end_date',
            'termination_date'              => 'termination_date',
            'termination_notes'             => 'termination_notes',
            'monthly_rent'                  => 'monthly_rent',
            'rent_security_deposit'         => 'rent_security_deposit',
            'rent_due_date'                 => 'rent_due_date',
            'pet_deposit'                   => 'pet_deposit',
            'electricity_deposit'           => 'electricity_deposit',
            'water_deposit'                 => 'water_deposit',
            'total_other_deposits'          => 'total_other_deposits',
            'other_deposits_description'    => 'other_deposits_description',
            'lease_status'                  => 'lease_status',
            'apply_fine_on_late_payment'    => 'apply_fine_on_late_payment',
            'served_by_staff_id'            => 'served_by_staff_id',
            'created_at'                    => 'created_at',
            'updated_at'                    => 'updated_at'
        ];

    }

    /**
     * Checks for reversed transformation. Only for terms ready for search.
     * @param $providedField
     * @return bool
     */
    public function reverse($providedField) {

        $availableFields = $this->fieldMap();

        return $this->reverseField($providedField, $availableFields);
    }

}