<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Laravel\Passport\Exceptions\MissingScopeException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isHttpException($exception)) {
            if ($exception instanceof NotFoundHttpException) {

                return response()->json( [
                    'error' => [
                        'error'         => true,
                        'message'       => 'Sorry, the resource you are looking for could not be found..',
                        'status_code'   => 404
                    ]], 404);
            }
        }


        if ($exception instanceof AuthorizationException) {

            return response()->json( [
                'error' => [
                    'error'         => true,
                    'message'       => 'You are not authorized to access this resource..',
                    'status_code'   => 403
                ]], 403);
        }


        if ($exception instanceof MissingScopeException) {

            return response()->json( [
                'error' => [
                    'error'         => true,
                    'message'       => 'You do not have permission to access this resource..',
                    'status_code'   => 403
                ]], 403);
        }


        if ($exception instanceof MethodNotAllowedHttpException) {

            return response()->json( [
                'error' => [
                    'error'         => true,
                    'message'       => 'Method is not allowed.',
                    'status_code'   => 405
                ]], 405);
        }


        if ($exception instanceof UnauthorizedHttpException) {

            return response()->json( [
                'error' => [
                    'error'         => true,
                    'message'       => 'Provided login credentials were incorrect ...',
                    'status_code'   => 401
                ]], 401);
        }


        if ($exception instanceof JsonEncodingException) {

            return response()->json( [
                'error' => [
                    'error'         => true,
                    'message'       => 'Invalid data provided ...',
                    'status_code'   => 400
                ]], 400);
        }

        if ($exception instanceof DecryptException) {

        return response()->json( [
            'error' => [
                'error'         => true,
                'message'       => 'The MAC is invalid. CHeck application keys',
                'status_code'   => 401
            ]], 401);
        }

        return parent::render($request, $exception);
    }
}
