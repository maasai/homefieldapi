<?php

namespace App\Http\Requests;

class PaymentCategoryRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'category_name'           => 'required|min:3',
                    'category_description'    => 'nullable|min:3',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'category_name'           => 'min:3',
                    'category_description'    => 'nullable|min:3',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
