<?php

namespace App\Http\Requests;

class PaymentMethodRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'method_name'           => 'required|min:3',
                    'method_description'    => 'nullable|min:3',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'method_name'           => 'min:3',
                    'method_description'    => 'nullable|min:3',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
