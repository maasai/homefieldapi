<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class CareTakerRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'first_name'                => 'required|min:2',
                    'last_name'                 => 'nullable|min:2',
                    'email'                     => 'required|email|unique:care_takers,email,NULL,uuid,deleted_at,NULL',
                    'phone'                     => 'required|regex:/(01)[0-9]{9}/',
                    'nationality'               => 'nullable|min:2',
                    'id_number'                 => 'nullable|min:2',
                    'kra_pin'                   => 'nullable|min:2',
                    'passport_number'           => 'nullable|min:2',
                    'postal_address'            => 'nullable|min:2',
                    'marital_status'            => 'nullable|min:2',
                    'emergency_full_name'       => 'nullable|min:2',
                    'emergency_postal_address'  => 'nullable|min:2',
                    'emergency_phone'           => 'nullable|min:2',
                    'emergency_email'           => 'nullable|min:2',
                    'first_referee_full_name'   => 'nullable|min:2',
                    'first_referee_phone'       => 'nullable|min:2',
                    'first_referee_address'     => 'nullable|min:2',
                    'first_referee_email'       => 'nullable|min:2',
                    'second_referee_full_name'  => 'nullable|min:2',
                    'second_referee_phone'      => 'nullable|min:2',
                    'second_referee_address'    => 'nullable|min:2',
                    'second_referee_email'      => 'nullable|min:2',
                    'third_referee_full_name'   => 'nullable|min:2',
                    'third_referee_phone'       => 'nullable|min:2',
                    'third_referee_address'     => 'nullable|min:2',
                    'third_referee_email'       => 'nullable|min:2',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'first_name'                => 'min:2',
                    'last_name'                 => 'nullable|min:2',
                    'email'                     => ['email', Rule::unique('care_takers')->ignore($this->care_taker, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'phone'                     => 'regex:/(01)[0-9]{9}/',
                    'nationality'               => 'nullable|min:2',
                    'id_number'                 => 'nullable|min:2',
                    'kra_pin'                   => 'nullable|min:2',
                    'passport_number'           => 'nullable|min:2',
                    'postal_address'            => 'nullable|min:2',
                    'marital_status'            => 'nullable|min:2',
                    'emergency_full_name'       => 'nullable|min:2',
                    'emergency_postal_address'  => 'nullable|min:2',
                    'emergency_phone'           => 'nullable|min:2',
                    'emergency_email'           => 'nullable|min:2',
                    'first_referee_full_name'   => 'nullable|min:2',
                    'first_referee_phone'       => 'nullable|min:2',
                    'first_referee_address'     => 'nullable|min:2',
                    'first_referee_email'       => 'nullable|min:2',
                    'second_referee_full_name'  => 'nullable|min:2',
                    'second_referee_phone'      => 'nullable|min:2',
                    'second_referee_address'    => 'nullable|min:2',
                    'second_referee_email'      => 'nullable|min:2',
                    'third_referee_full_name'   => 'nullable|min:2',
                    'third_referee_phone'       => 'nullable|min:2',
                    'third_referee_address'     => 'nullable|min:2',
                    'third_referee_email'       => 'nullable|min:2',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
