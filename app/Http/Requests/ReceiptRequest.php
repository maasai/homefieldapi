<?php

namespace App\Http\Requests;

class ReceiptRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [

                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [

                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
