<?php

namespace App\Http\Requests;

class PaymentRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'lease_id'              => 'required|exists:payments,uuid',
                    'tenant_id'             => 'required|exists:payments,uuid',
                    'invoice_id'            => 'required|exists:invoices,uuid',
                    'paid_by'               => 'required',
                    'reference_number'      => 'required',
                    'payment_date'          => 'required',
                    'amount'                => 'required',
                    'payment_method_id'     => 'required|exists:payment_methods,uuid',
                    'payment_category_id'   => 'required|exists:payment_categories,uuid',
                    'other_details'         => 'required',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'lease_id'              => 'exists:payments,uuid',
                    'tenant_id'             => 'exists:payments,uuid',
                    'invoice_id'            => 'exists:invoices,uuid',
                    'paid_by'               => 'min:3',
                    'reference_number'      => '',
                    'payment_date'          => 'date',
                    'amount'                => '',
                    'payment_method_id'     => 'exists:payment_methods,uuid',
                    'payment_category_id'   => 'exists:payment_categories,uuid',
                    'other_details'         => 'min3',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
