<?php

namespace App\Http\Requests;

class LeaseRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'tenant_id'                     => 'required|exists:tenants,uuid',
                    'apartment_id'                  => 'required|exists:apartments,uuid',
                    'house_unit_id'                 => 'required|exists:house_units,uuid',
                    'start_date'                    => 'required|min:2',
                    'end_date'                      => 'nullable|min:2',
                    'termination_date'              => 'nullable|min:2',
                    'termination_notes'             => 'nullable|min:2',
                    'monthly_rent'                  => 'required|min:2',
                    'rent_security_deposit'         => 'nullable|min:2',
                    'rent_due_date'                 => 'nullable|min:2',
                    'pet_deposit'                   => 'nullable|min:2',
                    'electricity_deposit'           => 'nullable|min:2',
                    'water_deposit'                 => 'nullable|min:2',
                    'total_other_deposits'          => 'nullable|min:2',
                    'other_deposits_description'    => 'nullable|min:2',
                    'lease_status'                  => 'nullable|min:2',
                    'apply_fine_on_late_payment'    => 'nullable|min:2',
                    'served_by_staff_id'            => 'nullable|exists:users,uuid'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'tenant_id'                     => 'exists:tenants,uuid',
                    'apartment_id'                  => 'exists:apartments,uuid',
                    'house_unit_id'                 => 'exists:house_units,uuid',
                    'start_date'                    => 'min:2',
                    'end_date'                      => 'nullable|min:2',
                    'termination_date'              => 'nullable|min:2',
                    'termination_notes'             => 'nullable|min:2',
                    'monthly_rent'                  => 'min:2',
                    'rent_security_deposit'         => 'nullable|min:2',
                    'rent_due_date'                 => 'nullable|min:2',
                    'pet_deposit'                   => 'nullable|min:2',
                    'electricity_deposit'           => 'nullable|min:2',
                    'water_deposit'                 => 'nullable|min:2',
                    'total_other_deposits'          => 'nullable|min:2',
                    'other_deposits_description'    => 'nullable|min:2',
                    'lease_status'                  => 'nullable|min:2',
                    'apply_fine_on_late_payment'    => 'nullable|min:2',
                    'served_by_staff_id'            => 'nullable|exists:users,uuid'
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
