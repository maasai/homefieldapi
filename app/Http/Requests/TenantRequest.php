<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class TenantRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'user_id'                           => 'required|exists:users,uuid',
                    'nationality'                       => 'nullable',
                    'id_number'                         => 'nullable|unique:tenants,id_number,NULL,uuid,deleted_at,NULL',
                    'passport_number'                   => 'nullable|unique:tenants,passport_number,NULL,uuid,deleted_at,NULL',
                    'postal_address'                    => 'nullable',
                    'marital_status'                    => 'nullable',
                    'number_of_intended_occupants'      => 'nullable',
                    'occupant_relationship'             => 'nullable',
                    'emergency_full_name'               => 'nullable',
                    'emergency_postal_address'          => 'nullable',
                    'emergency_phone'                   => 'nullable',
                    'emergency_email'                   => 'nullable',
                    'employment_status'                 => 'nullable',
                    'employment_employer'               => 'nullable',
                    'employment_position_held'          => 'nullable',
                    'employment_physical_address'       => 'nullable',
                    'employment_phone'                  => 'nullable',
                    'self_employed_business'            => 'nullable',
                    'self_employed_business_address'    => 'nullable',
                    'self_rent_payer'                   => 'nullable',
                    'rent_payer'                        => 'nullable',
                    'rent_payer_contact'                => 'nullable',
                    'date_joined'                       => 'nullable',
                    'commencement_of_lease'             => 'nullable',
                    'kra_pin'                           => 'nullable',
                    'passport_photo_url'                => 'nullable'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'user_id'                           => 'exists:users,uuid',
                    'nationality'                       => 'nullable',
                    'id_number'                         => [Rule::unique('tenants')->ignore($this->tenant, 'uuid') ],
                    'passport_number'                   => [Rule::unique('tenants')->ignore($this->tenant, 'uuid') ],
                    'postal_address'                    => 'nullable',
                    'marital_status'                    => 'nullable',
                    'number_of_intended_occupants'      => 'nullable',
                    'occupant_relationship'             => 'nullable',
                    'emergency_full_name'               => 'nullable',
                    'emergency_postal_address'          => 'nullable',
                    'emergency_phone'                   => 'nullable',
                    'emergency_email'                   => 'nullable',
                    'employment_status'                 => 'nullable',
                    'employment_employer'               => 'nullable',
                    'employment_position_held'          => 'nullable',
                    'employment_physical_address'       => 'nullable',
                    'employment_phone'                  => 'nullable',
                    'self_employed_business'            => 'nullable',
                    'self_employed_business_address'    => 'nullable',
                    'self_rent_payer'                   => 'nullable',
                    'rent_payer'                        => 'nullable',
                    'rent_payer_contact'                => 'nullable',
                    'date_joined'                       => 'nullable',
                    'commencement_of_lease'             => 'nullable',
                    'kra_pin'                           => 'nullable',
                    'passport_photo_url'                => 'nullable'
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
