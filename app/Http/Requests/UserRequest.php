<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class UserRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'first_name'            => 'required|min:2',
                    'last_name'             => 'nullable|min:2',
                    'email'                 => 'required|email|unique:users,email,NULL,uuid,deleted_at,NULL',
                    'salutation'            => 'nullable|max:4',
                    'phone'                 => 'nullable|regex:/(01)[0-9]{9}/',
                    'address'               => 'nullable',
                    'password'              => 'nullable|min:3|confirmed',
                    'password_confirmation' => 'nullable',
                    'role_id'               => 'required|exists:roles,uuid',
                    'profile_picture_url'   => 'nullable'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'first_name'            => 'min:2',
                    'last_name'             => 'nullable|min:2',
                   //'email'                 => [ 'email', Rule::unique('users')->ignore($this->user, 'uuid') ],
                    'email'         => ['email', Rule::unique('users')->ignore($this->user, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'salutation'            => 'nullable|max:4',
                    'phone'                 => 'nullable|regex:/(01)[0-9]{9}/',
                    'address'               => 'nullable',
                    'password'              => 'min:3|confirmed',
                    'password_confirmation' => 'required_with:password',
                    'role_id'               => 'exists:roles,uuid',
                    'profile_picture_url'   => 'nullable|url'

                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
