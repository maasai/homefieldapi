<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class HouseUnitRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'apartment_id'          => 'required|exists:apartments,uuid',
                    'house_number'          => 'required|unique:house_units,house_number,NULL,uuid,deleted_at,NULL',
                    'bedroom_count'         => 'nullable|min:2',
                    'bathroom_count'        => 'nullable|min:2',
                    'garage_attached'       => 'nullable|min:2',
                    'area_sq_feet'          => 'nullable|min:2',
                    'unit_status'           => 'nullable|min:2',
                    'commercial'            => 'nullable|min:2',
                    'parking_available'     => 'nullable|min:2',
                    'balcony_available'     => 'nullable|min:2',
                    'floor_type'            => 'nullable|min:2',
                    'notes'                 => 'nullable|min:2'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'apartment_id'          => 'exists:apartments,uuid',
                    'house_number'          => [Rule::unique('house_units')->ignore($this->house_unit, 'uuid') ],
                    'bedroom_count'         => 'nullable|min:2',
                    'bathroom_count'        => 'nullable|min:2',
                    'garage_attached'       => 'nullable|min:2',
                    'area_sq_feet'          => 'nullable|min:2',
                    'unit_status'           => 'nullable|min:2',
                    'commercial'            => 'nullable|min:2',
                    'parking_available'     => 'nullable|min:2',
                    'balcony_available'     => 'nullable|min:2',
                    'floor_type'            => 'nullable|min:2',
                    'notes'                 => 'nullable|min:2'
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
