<?php


namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class RoleRequest extends JsonRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'role_name'           => 'required|min:3|unique:roles,role_name,NULL,uuid,deleted_at,NULL',
                    'role_display_name'   => 'required|min:3|unique:roles,role_display_name,NULL,uuid,deleted_at,NULL',
                    'role_description'    => 'nullable|min:3'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'role_name'         => [Rule::unique('roles')->ignore($this->role, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'role_display_name'         => ['min:3', Rule::unique('roles')->ignore($this->role, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'role_description'    => 'nullable|min:3'

                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
