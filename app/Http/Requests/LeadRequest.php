<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class LeadRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'full_name'         => 'required|min:2',
                    'phone'             => 'required|unique:leads,phone,NULL,uuid,deleted_at,NULL',
                    'email'             => 'nullable|email|unique:leads,email,NULL,uuid,deleted_at,NULL',
                    'service_needed'    => 'required|min:2',
                    'address'           => 'nullable|min:2',
                    'budget'            => 'nullable|min:2',
                    'gender'            => 'nullable|min:2',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'full_name'         => 'required|min:2',
                    'email'         => ['email', Rule::unique('leads')->ignore($this->lead, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'phone'         => [Rule::unique('leads')->ignore($this->lead, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'service_needed'    => 'required|min:2',
                    'address'           => 'nullable|min:2',
                    'budget'            => 'nullable|min:2',
                    'gender'            => 'nullable|min:2',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
