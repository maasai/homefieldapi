<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class PermissionRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'permission_name'           => 'required|min:3|unique:permissions,permission_name,NULL,uuid,deleted_at,NULL',
                    'permission_display_name'   => 'required|min:3|unique:permissions,permission_display_name,NULL,uuid,deleted_at,NULL',
                    'permission_description'    => 'nullable|min:3'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                   // 'permission_name'           =>  ['min:3', Rule::unique('permissions')->ignore($this->permission, 'uuid') ],
                    'permission_name'         => ['min:3', Rule::unique('permissions')->ignore($this->permission, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],

                   // 'permission_display_name'   =>  [ 'min:3', Rule::unique('permissions')->ignore($this->permission, 'uuid') ],
                    'permission_display_name'         => ['min:3', Rule::unique('permissions')->ignore($this->permission, 'uuid')
                        ->where(function ($query) {
                            $query->where('deleted_at', NULL);
                        })],
                    'permission_description'    => 'nullable|min:3'

                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
