<?php

namespace App\Http\Requests;

class InvoiceStatusRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'status_name'           => 'required|min:3',
                    'status_description'    => 'nullable|min:3',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'status_name'           => 'min:3',
                    'status_description'    => 'nullable|min:3',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
