<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class ApartmentRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'landlord_id'                       => 'required|exists:users,uuid',
                    'operations_officer_incharge_id'    => 'nullable|exists:users,uuid',
                    'care_taker_id'                     => 'nullable|exists:care_takers,uuid',
                    'apartment_name'                    => 'required|unique:apartments,apartment_name,NULL,uuid,deleted_at,NULL',
                    'apartment_reference_number'        => 'required|unique:apartments,apartment_reference_number,NULL,uuid,deleted_at,NULL',
                    'address'                           => 'required|min:2',
                    'geo'                               => 'nullable|min:2',
                    'smoking_allowed'                   => 'nullable|min:2',
                    'pets_allowed'                      => 'nullable|min:2',
                    'total_single_units'                => 'nullable|min:2',
                    'total_bedsitters'                  => 'nullable|min:2',
                    'total_one_bedrooms'                => 'nullable|min:2',
                    'total_two_bedrooms'                => 'nullable|min:2',
                    'total_three_bedrooms'              => 'nullable|min:2',
                    'total_four_bedrooms'               => 'nullable|min:2',
                    'total_commercial_rooms'            => 'nullable|min:2',
                    'extra_details'                     => 'nullable|min:2'
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'landlord_id'                       => 'exists:users,uuid',
                    'operations_officer_incharge_id'    => 'nullable|exists:users,uuid',
                    'care_taker_id'                     => 'nullable|exists:care_takers,uuid',
                    'apartment_name'                    => [Rule::unique('apartments')->ignore($this->apartment, 'uuid') ],
                    'apartment_reference_number'        => [Rule::unique('apartments')->ignore($this->apartment_reference_number, 'uuid') ],
                    'address'                           => 'min:2',
                    'geo'                               => 'nullable|min:2',
                    'smoking_allowed'                   => 'nullable|min:2',
                    'pets_allowed'                      => 'nullable|min:2',
                    'total_single_units'                => 'nullable|min:2',
                    'total_bedsitters'                  => 'nullable|min:2',
                    'total_one_bedrooms'                => 'nullable|min:2',
                    'total_two_bedrooms'                => 'nullable|min:2',
                    'total_three_bedrooms'              => 'nullable|min:2',
                    'total_four_bedrooms'               => 'nullable|min:2',
                    'total_commercial_rooms'            => 'nullable|min:2',
                    'extra_details'                     => 'nullable|min:2'
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
