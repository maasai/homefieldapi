<?php

namespace App\Http\Requests;

class ApartmentPaymentOptionRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
                break;
            }
            case 'POST':
            {
                $rules = [
                    'apartment_id'              => 'required|exists,apartments,uuid',
                    'payment_option_details'    => 'nullable|min:3',
                ];

                break;
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules = [
                    'apartment_id'              => 'exists,apartments,uuid',
                    'payment_option_details'    => 'nullable|min:3',
                ];
                break;
            }
            default:break;
        }

        return $rules;

    }
}
