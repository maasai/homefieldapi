<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\TenantRequest;
use App\HomeField\Repositories\Contracts\TenantInterface;
use App\HomeField\Transformers\TenantTransformer;
use Illuminate\Http\Request;

class TenantController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\TenantInterface
     */
    protected $tenantRepository, $tenantTransformer;

    /**
     * @param TenantInterface $tenantRepository
     * @param TenantTransformer $tenantTransformer
     */
    public function __construct(TenantInterface $tenantRepository, TenantTransformer $tenantTransformer)
    {
        $this->tenantRepository   = $tenantRepository;
        $this->tenantTransformer  = $tenantTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->tenantRepository, $this->tenantTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->tenantRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->tenantTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param TenantRequest $request
     * @return mixed
     */
    public function store(TenantRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->tenantRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $tenant = $this->tenantRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $tenant )
                //event(new TenantCreated($tenant));

                return $this->respondWithSuccess('Success !! Tenant has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $tenant = $this->tenantRepository->getById($uuid);

        if(!$tenant)
        {
            return $this->respondNotFound('Tenant not found.');
        }

        return $this->respond([
            'data' => $this->tenantTransformer->transform($tenant)
        ]);
    }
    /**
     * @param TenantRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(TenantRequest $request, $uuid)
    {
        $save = $this->tenantRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Tenant has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->tenantRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Tenant has been deleted');
        }
        return $this->respondNotFound('Tenant not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->tenantRepository, $this->tenantTransformer);

    }
}