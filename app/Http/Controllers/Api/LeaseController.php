<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\LeaseRequest;
use App\HomeField\Repositories\Contracts\LeaseInterface;
use App\HomeField\Transformers\LeaseTransformer;
use Illuminate\Http\Request;

class LeaseController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\LeaseInterface
     */
    protected $leaseRepository, $leaseTransformer;

    /**
     * @param LeaseInterface $leaseRepository
     * @param LeaseTransformer $leaseTransformer
     */
    public function __construct(LeaseInterface $leaseRepository, LeaseTransformer $leaseTransformer)
    {
        $this->leaseRepository   = $leaseRepository;
        $this->leaseTransformer  = $leaseTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->leaseRepository, $this->leaseTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->leaseRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->leaseTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param LeaseRequest $request
     * @return mixed
     */
    public function store(LeaseRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->leaseRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $lease = $this->leaseRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $lease )
                //event(new LeaseCreated($lease));

                return $this->respondWithSuccess('Success !! Lease has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $lease = $this->leaseRepository->getById($uuid);

        if(!$lease)
        {
            return $this->respondNotFound('Lease not found.');
        }

        return $this->respond([
            'data' => $this->leaseTransformer->transform($lease)
        ]);
    }
    /**
     * @param LeaseRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(LeaseRequest $request, $uuid)
    {
        $save = $this->leaseRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Lease has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->leaseRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Lease has been deleted');
        }
        return $this->respondNotFound('Lease not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->leaseRepository, $this->leaseTransformer);

    }
}