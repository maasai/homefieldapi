<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\PaymentCategoryRequest;
use App\HomeField\Repositories\Contracts\PaymentCategoryInterface;
use App\HomeField\Transformers\PaymentCategoryTransformer;
use Illuminate\Http\Request;

class PaymentCategoryController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\PaymentCategoryInterface
     */
    protected $paymentCategoryRepository, $paymentCategoryTransformer;

    /**
     * @param PaymentCategoryInterface $paymentCategoryRepository
     * @param PaymentCategoryTransformer $paymentCategoryTransformer
     */
    public function __construct(PaymentCategoryInterface $paymentCategoryRepository, PaymentCategoryTransformer $paymentCategoryTransformer)
    {
        $this->paymentCategoryRepository   = $paymentCategoryRepository;
        $this->paymentCategoryTransformer  = $paymentCategoryTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->paymentCategoryRepository, $this->paymentCategoryTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->paymentCategoryRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->paymentCategoryTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param PaymentCategoryRequest $request
     * @return mixed
     */
    public function store(PaymentCategoryRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->paymentCategoryRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $paymentCategory = $this->paymentCategoryRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $paymentCategory )
                //event(new PaymentCategoryCreated($paymentCategory));

                return $this->respondWithSuccess('Success !! PaymentCategory has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $paymentCategory = $this->paymentCategoryRepository->getById($uuid);

        if(!$paymentCategory)
        {
            return $this->respondNotFound('PaymentCategory not found.');
        }

        return $this->respond([
            'data' => $this->paymentCategoryTransformer->transform($paymentCategory)
        ]);
    }
    /**
     * @param PaymentCategoryRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(PaymentCategoryRequest $request, $uuid)
    {
        $save = $this->paymentCategoryRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! PaymentCategory has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->paymentCategoryRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! PaymentCategory has been deleted');
        }
        return $this->respondNotFound('PaymentCategory not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->paymentCategoryRepository, $this->paymentCategoryTransformer);

    }
}