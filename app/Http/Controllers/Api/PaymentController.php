<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\PaymentRequest;
use App\HomeField\Repositories\Contracts\PaymentInterface;
use App\HomeField\Transformers\PaymentTransformer;
use Illuminate\Http\Request;

class PaymentController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\PaymentInterface
     */
    protected $paymentRepository, $paymentTransformer;

    /**
     * @param PaymentInterface $paymentRepository
     * @param PaymentTransformer $paymentTransformer
     */
    public function __construct(PaymentInterface $paymentRepository, PaymentTransformer $paymentTransformer)
    {
        $this->paymentRepository   = $paymentRepository;
        $this->paymentTransformer  = $paymentTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->paymentRepository, $this->paymentTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->paymentRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->paymentTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param PaymentRequest $request
     * @return mixed
     */
    public function store(PaymentRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->paymentRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $payment = $this->paymentRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $payment )
                //event(new PaymentCreated($payment));

                return $this->respondWithSuccess('Success !! Payment has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $payment = $this->paymentRepository->getById($uuid);

        if(!$payment)
        {
            return $this->respondNotFound('Payment not found.');
        }

        return $this->respond([
            'data' => $this->paymentTransformer->transform($payment)
        ]);
    }
    /**
     * @param PaymentRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(PaymentRequest $request, $uuid)
    {
        $save = $this->paymentRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Payment has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->paymentRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Payment has been deleted');
        }
        return $this->respondNotFound('Payment not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->paymentRepository, $this->paymentTransformer);

    }
}