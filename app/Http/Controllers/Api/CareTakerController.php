<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\CareTakerRequest;
use App\HomeField\Repositories\Contracts\CareTakerInterface;
use App\HomeField\Transformers\CareTakerTransformer;
use Illuminate\Http\Request;

class CareTakerController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\CareTakerInterface
     */
    protected $careTakerRepository, $careTakerTransformer;

    /**
     * @param CareTakerInterface $careTakerRepository
     * @param CareTakerTransformer $careTakerTransformer
     */
    public function __construct(CareTakerInterface $careTakerRepository, CareTakerTransformer $careTakerTransformer)
    {
        $this->careTakerRepository   = $careTakerRepository;
        $this->careTakerTransformer  = $careTakerTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->careTakerRepository, $this->careTakerTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->careTakerRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->careTakerTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param CareTakerRequest $request
     * @return mixed
     */
    public function store(CareTakerRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->careTakerRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $careTaker = $this->careTakerRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $careTaker )
                //event(new BuildingCreated($careTaker));

                return $this->respondWithSuccess('Success !! Building has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $careTaker = $this->careTakerRepository->getById($uuid);

        if(!$careTaker)
        {
            return $this->respondNotFound('Building not found.');
        }

        return $this->respond([
            'data' => $this->careTakerTransformer->transform($careTaker)
        ]);
    }
    /**
     * @param CareTakerRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(CareTakerRequest $request, $uuid)
    {
        $save = $this->careTakerRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Building has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->careTakerRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Building has been deleted');
        }
        return $this->respondNotFound('Building not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->careTakerRepository, $this->careTakerTransformer);

    }
}