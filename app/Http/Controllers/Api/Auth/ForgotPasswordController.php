<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends ApiController
{
    /*
        |--------------------------------------------------------------------------
        | Password Reset Controller
        |--------------------------------------------------------------------------
        |
        | This controller is responsible for handling password reset emails and
        | includes a trait which assists in sending these notifications from
        | your application to your users. Feel free to explore this trait.
        |
         */
    use SendsPasswordResetEmails;

    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        if ($request->wantsJson()) {
            $user = User::where('email', $request->input('email'))->first();

            if (!$user) {
              //  return response()->json(Json::response(null, trans('passwords.user')), 400);
                return $this->respondWrongParameter('Email not found in system');
            }

            $token = $this->broker()->createToken($user);
            $data = ['token' => $token, 'message' => "Reset toke has been generated" ];
                return $this->respond($data);

        }

        return $this->respondNotFound();
    }
}
