<?php


namespace App\Http\Controllers\Api\Auth;


use App\Cmmq\Repositories\Contracts\RoleInterface;
use App\Cmmq\Repositories\Contracts\UserInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Application;
use League\Flysystem\Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class LoginProxy
{
    const REFRESH_TOKEN = 'refreshToken';

    private $auth;

    private $cookie;

    private $db;

    private $request;

    private $userRepository, $roleRepository;

    /**
     * LoginProxy constructor.
     * @param Application $app
     * @param UserInterface $userRepository
     * @param RoleInterface $roleRepository
     */
    public function __construct(Application $app, UserInterface $userRepository, RoleInterface $roleRepository) {

        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        $this->auth = $app->make('auth');
        $this->cookie = $app->make('cookie');
        $this->db = $app->make('db');
        $this->request = $app->make('request');
    }


    /**
     * @param $email
     * @param $password
     * @return array|\Illuminate\Http\JsonResponse|mixed
     * @throws Exception
     */
    public function attemptLogin($email, $password)
    {
        $user = $this->userRepository->getWhere('email', $email);

        if (!is_null($user)) {

            //$scope = trim($this->checkPermissions($user->role_id));
            $scope = trim($this->checkRole($user->role_id));

            return $this->proxy('password', [
                'username'  => $email,
                'password'  => $password,
                'scope'     => $scope ? $scope : 'null'
            ]);
        }

        throw new UnauthorizedHttpException(null, Exception::class, null, 0);
    }


    /**
     * Attempt to refresh the access token used a refresh token that
     * has been saved in a cookie
     */
    public function attemptRefresh()
    {
        try{
            $refreshToken = $this->request->cookie(self::REFRESH_TOKEN);

            return $this->proxy('refresh_token', [
                'refresh_token' => decrypt($refreshToken)
            ]);

        }catch (DecryptException $e){

            throw new DecryptException($e);

        }
    }


    /**
     * @param $grantType
     * @param array $data
     * @return array|\Illuminate\Http\JsonResponse|mixed
     */
    private function proxy($grantType, array $data = [])
    {
        try {
            $config = app()->make('config');

            $data = array_merge([

                    'client_id'     => env('PASSWORD_CLIENT_ID'),
                    'client_secret' => env('PASSWORD_CLIENT_SECRET'),
                    'grant_type'    => $grantType

            ], $data);

            $client = new Client();

            $guzzleResponse = $client->request('POST', sprintf('%s/oauth/token', $config->get('app.url')), [
                'form_params' => $data
            ]);

        } catch(BadResponseException $e) {
            $guzzleResponse = $e->getResponse();

        }

        $response = json_decode($guzzleResponse->getBody());

        if (property_exists($response, "access_token")) {
            $cookie = app()->make('cookie');
            $crypt  = app()->make('encrypter');

            $encryptedToken = $crypt->encrypt($response->refresh_token);

            // Set the refresh token as an encrypted HttpOnly cookie
                $cookie->queue('refreshToken',
               // $crypt->encrypt($encryptedToken),
                $crypt->encrypt($response->refresh_token),
                604800, // expiration, should be moved to a config file
                null,
                null,
                false,
                true // HttpOnly
            );

            $response = [
                'accessToken'            => $response->access_token,
                'accessTokenExpiration'  => $response->expires_in
            ];
        }

        $response = response()->json($response);
        $response->setStatusCode($guzzleResponse->getStatusCode());

        $headers = $guzzleResponse->getHeaders();
        foreach($headers as $headerType => $headerValue) {
            $response->header($headerType, $headerValue);
        }

        return $response;
    }

    /**
     * Logs out the user. We revoke access token and refresh token.
     * Also instruct the client to forget the refresh cookie.
     */
    public function logout()
    {
        $accessToken = $this->auth->user()->token();

        $refreshToken = $this->db
            ->table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        $this->cookie->queue($this->cookie->forget(self::REFRESH_TOKEN));
    }


    /**
     * @param $roleId
     * @return string
     */
    private function checkPermissions($roleId)
    {
        $role = $this->roleRepository->getWhere('uuid', $roleId, ['permissions']);

        $role_permissions = $role->permissions()->get()->toArray();
        $data = [];

        foreach ($role_permissions as $key => $value){
            $data[] = trim($value['permission_name']);
        }

        return implode(' ', $data);
    }


    /**
     * @param $roleId
     * @return string
     */
    private function checkRole($roleId)
    {
        $role = $this->roleRepository->getWhere('uuid', $roleId, ['permissions']);
        $data[] = trim(strtolower($role->role_name));

        return implode(' ', $data);
    }

}