<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\ApartmentPaymentOptionRequest;
use App\HomeField\Repositories\Contracts\ApartmentPaymentOptionInterface;
use App\HomeField\Transformers\ApartmentPaymentOptionTransformer;
use Illuminate\Http\Request;

class ApartmentPaymentOptionController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\ApartmentPaymentOptionInterface
     */
    protected $apartmentPaymentOptionRepository, $apartmentPaymentOptionTransformer;

    /**
     * @param ApartmentPaymentOptionInterface $apartmentPaymentOptionRepository
     * @param ApartmentPaymentOptionTransformer $apartmentPaymentOptionTransformer
     */
    public function __construct(ApartmentPaymentOptionInterface $apartmentPaymentOptionRepository, ApartmentPaymentOptionTransformer $apartmentPaymentOptionTransformer)
    {
        $this->apartmentPaymentOptionRepository   = $apartmentPaymentOptionRepository;
        $this->apartmentPaymentOptionTransformer  = $apartmentPaymentOptionTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->apartmentPaymentOptionRepository, $this->apartmentPaymentOptionTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->apartmentPaymentOptionRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->apartmentPaymentOptionTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param ApartmentPaymentOptionRequest $request
     * @return mixed
     */
    public function store(ApartmentPaymentOptionRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->apartmentPaymentOptionRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $apartmentPaymentOption = $this->apartmentPaymentOptionRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $apartmentPaymentOption )
                //event(new BuildingCreated($apartmentPaymentOption));

                return $this->respondWithSuccess('Success !! Building has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $apartmentPaymentOption = $this->apartmentPaymentOptionRepository->getById($uuid);

        if(!$apartmentPaymentOption)
        {
            return $this->respondNotFound('Building not found.');
        }

        return $this->respond([
            'data' => $this->apartmentPaymentOptionTransformer->transform($apartmentPaymentOption)
        ]);
    }
    /**
     * @param ApartmentPaymentOptionRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ApartmentPaymentOptionRequest $request, $uuid)
    {
        $save = $this->apartmentPaymentOptionRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Building has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->apartmentPaymentOptionRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Building has been deleted');
        }
        return $this->respondNotFound('Building not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->apartmentPaymentOptionRepository, $this->apartmentPaymentOptionTransformer);

    }
}