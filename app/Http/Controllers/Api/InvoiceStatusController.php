<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\InvoiceStatusRequest;
use App\HomeField\Repositories\Contracts\InvoiceStatusInterface;
use App\HomeField\Transformers\InvoiceStatusTransformer;
use Illuminate\Http\Request;

class InvoiceStatusController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\InvoiceStatusInterface
     */
    protected $invoiceStatusRepository, $invoiceStatusTransformer;

    /**
     * @param InvoiceStatusInterface $invoiceStatusRepository
     * @param InvoiceStatusTransformer $invoiceStatusTransformer
     */
    public function __construct(InvoiceStatusInterface $invoiceStatusRepository, InvoiceStatusTransformer $invoiceStatusTransformer)
    {
        $this->invoiceStatusRepository   = $invoiceStatusRepository;
        $this->invoiceStatusTransformer  = $invoiceStatusTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->invoiceStatusRepository, $this->invoiceStatusTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->invoiceStatusRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->invoiceStatusTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param InvoiceStatusRequest $request
     * @return mixed
     */
    public function store(InvoiceStatusRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->invoiceStatusRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $invoiceStatus = $this->invoiceStatusRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $invoiceStatus )
                //event(new InvoiceStatusCreated($invoiceStatus));

                return $this->respondWithSuccess('Success !! InvoiceStatus has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $invoiceStatus = $this->invoiceStatusRepository->getById($uuid);

        if(!$invoiceStatus)
        {
            return $this->respondNotFound('InvoiceStatus not found.');
        }

        return $this->respond([
            'data' => $this->invoiceStatusTransformer->transform($invoiceStatus)
        ]);
    }
    /**
     * @param InvoiceStatusRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(InvoiceStatusRequest $request, $uuid)
    {
        $save = $this->invoiceStatusRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! InvoiceStatus has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->invoiceStatusRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! InvoiceStatus has been deleted');
        }
        return $this->respondNotFound('InvoiceStatus not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->invoiceStatusRepository, $this->invoiceStatusTransformer);

    }
}