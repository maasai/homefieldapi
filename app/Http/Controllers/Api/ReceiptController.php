<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\ReceiptRequest;
use App\HomeField\Repositories\Contracts\ReceiptInterface;
use App\HomeField\Transformers\ReceiptTransformer;
use Illuminate\Http\Request;

class ReceiptController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\ReceiptInterface
     */
    protected $receiptRepository, $receiptTransformer;

    /**
     * @param ReceiptInterface $receiptRepository
     * @param ReceiptTransformer $receiptTransformer
     */
    public function __construct(ReceiptInterface $receiptRepository, ReceiptTransformer $receiptTransformer)
    {
        $this->receiptRepository   = $receiptRepository;
        $this->receiptTransformer  = $receiptTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->receiptRepository, $this->receiptTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->receiptRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->receiptTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param ReceiptRequest $request
     * @return mixed
     */
    public function store(ReceiptRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->receiptRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $receipt = $this->receiptRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $receipt )
                //event(new ReceiptCreated($receipt));

                return $this->respondWithSuccess('Success !! Receipt has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $receipt = $this->receiptRepository->getById($uuid);

        if(!$receipt)
        {
            return $this->respondNotFound('Receipt not found.');
        }

        return $this->respond([
            'data' => $this->receiptTransformer->transform($receipt)
        ]);
    }
    /**
     * @param ReceiptRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ReceiptRequest $request, $uuid)
    {
        $save = $this->receiptRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Receipt has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->receiptRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Receipt has been deleted');
        }
        return $this->respondNotFound('Receipt not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->receiptRepository, $this->receiptTransformer);

    }
}