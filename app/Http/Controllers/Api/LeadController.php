<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\LeadRequest;
use App\HomeField\Repositories\Contracts\LeadInterface;
use App\HomeField\Transformers\LeadTransformer;
use Illuminate\Http\Request;

class LeadController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\LeadInterface
     */
    protected $leadRepository, $leadTransformer;

    /**
     * @param LeadInterface $leadRepository
     * @param LeadTransformer $leadTransformer
     */
    public function __construct(LeadInterface $leadRepository, LeadTransformer $leadTransformer)
    {
        $this->leadRepository   = $leadRepository;
        $this->leadTransformer  = $leadTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->leadRepository, $this->leadTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->leadRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->leadTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param LeadRequest $request
     * @return mixed
     */
    public function store(LeadRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->leadRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $lead = $this->leadRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $lead )
                //event(new LeadCreated($lead));

                return $this->respondWithSuccess('Success !! Lead has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $lead = $this->leadRepository->getById($uuid);

        if(!$lead)
        {
            return $this->respondNotFound('Lead not found.');
        }

        return $this->respond([
            'data' => $this->leadTransformer->transform($lead)
        ]);
    }
    /**
     * @param LeadRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(LeadRequest $request, $uuid)
    {
        $save = $this->leadRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Lead has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->leadRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Lead has been deleted');
        }
        return $this->respondNotFound('Lead not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->leadRepository, $this->leadTransformer);

    }
}