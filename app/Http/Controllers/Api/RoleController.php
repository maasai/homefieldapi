<?php


namespace App\Http\Controllers\Api;

use App\Http\Requests\RoleRequest;
use App\HomeField\Repositories\Contracts\RoleInterface;
use App\HomeField\Transformers\RoleTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\RoleInterface
     */
    protected $roleRepository, $roleTransformer;

    /**
     * @param RoleInterface $roleRepository
     * @param RoleTransformer $roleTransformer
     */

    public function __construct(RoleInterface $roleRepository, RoleTransformer $roleTransformer)
    {
        $this->roleRepository   = $roleRepository;
        $this->roleTransformer  = $roleTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->roleRepository, $this->roleTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = ['permissions'];

        $data = $this->roleRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->roleTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param RoleRequest $request
     * @return mixed
     */
    public function store(RoleRequest $request)
    {
        $data = $request->json()->all();

       // $save = $this->roleRepository->create($request->all());
        $save = $this->roleRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $role = $save['message'];

            if(array_key_exists('permission', $data)){
                $permissions = $data['permission'];

                if (!is_null($permissions)){
                   $role->permissions()->attach($permissions);
                }

            }

            return $this->respondWithSuccess('Success !! Role has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $role = $this->roleRepository->getById($uuid);

        if(!$role)
        {
            return $this->respondNotFound('Role not found.');
        }

        return $this->respond([
            'data' => $this->roleTransformer->transform($role)
        ]);
    }
    /**
     * @param RoleRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(RoleRequest $request, $uuid)
    {
        $data = $request->json()->all();

       // $save = $this->roleRepository->update($request->all(), $uuid);
        $save = $this->roleRepository->update($data, $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $role = $save['message'];

            if(array_key_exists('permission', $data)){
                $permissions = $data['permission'];

                if (!is_null($permissions)){
                    $role->permissions()->sync($permissions);
                }

            }else{
                return $this->respondNotSaved("Permission data not provided. See documentation for json format");
            }

            return $this->respondWithSuccess('Success !! Role has been updated.');
        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->roleRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Role has been deleted');
        }
        return $this->respondNotFound('Role not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->roleRepository, $this->roleTransformer);

    }
}