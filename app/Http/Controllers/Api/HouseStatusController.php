<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\HouseStatusRequest;
use App\HomeField\Repositories\Contracts\HouseStatusInterface;
use App\HomeField\Transformers\HouseStatusTransformer;
use Illuminate\Http\Request;

class HouseStatusController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\HouseStatusInterface
     */
    protected $houseStatusRepository, $houseStatusTransformer;

    /**
     * @param HouseStatusInterface $houseStatusRepository
     * @param HouseStatusTransformer $houseStatusTransformer
     */
    public function __construct(HouseStatusInterface $houseStatusRepository, HouseStatusTransformer $houseStatusTransformer)
    {
        $this->houseStatusRepository   = $houseStatusRepository;
        $this->houseStatusTransformer  = $houseStatusTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->houseStatusRepository, $this->houseStatusTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->houseStatusRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->houseStatusTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param HouseStatusRequest $request
     * @return mixed
     */
    public function store(HouseStatusRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->houseStatusRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $houseStatus = $this->houseStatusRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $houseStatus )
                //event(new HouseStatusCreated($houseStatus));

                return $this->respondWithSuccess('Success !! HouseStatus has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $houseStatus = $this->houseStatusRepository->getById($uuid);

        if(!$houseStatus)
        {
            return $this->respondNotFound('HouseStatus not found.');
        }

        return $this->respond([
            'data' => $this->houseStatusTransformer->transform($houseStatus)
        ]);
    }
    /**
     * @param HouseStatusRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(HouseStatusRequest $request, $uuid)
    {
        $save = $this->houseStatusRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! HouseStatus has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->houseStatusRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! HouseStatus has been deleted');
        }
        return $this->respondNotFound('HouseStatus not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->houseStatusRepository, $this->houseStatusTransformer);

    }
}