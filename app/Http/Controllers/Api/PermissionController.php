<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\PermissionRequest;
use App\HomeField\Repositories\Contracts\PermissionInterface;
use App\HomeField\Transformers\PermissionTransformer;
use Illuminate\Http\Request;

class PermissionController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\PermissionInterface
     */
    protected $permissionRepository, $permissionTransformer;

    /**
     * @param PermissionInterface $permissionRepository
     * @param PermissionTransformer $permissionTransformer
     */

    public function __construct(PermissionInterface $permissionRepository, PermissionTransformer $permissionTransformer)
    {
       // $this->middleware('scopes:view-users');

        $this->permissionRepository   = $permissionRepository;
        $this->permissionTransformer  = $permissionTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->permissionRepository, $this->permissionTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->permissionRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->permissionTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param PermissionRequest $request
     * @return mixed
     */
    public function store(PermissionRequest $request)
    {
        $save = $this->permissionRepository->create($request->all());

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Permission has been created.');
    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $permission = $this->permissionRepository->getById($uuid);

        if(!$permission)
        {
            return $this->respondNotFound('Permission not found.');
        }

        return $this->respond([
            'data' => $this->permissionTransformer->transform($permission)
        ]);
    }
    /**
     * @param PermissionRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(PermissionRequest $request, $uuid)
    {
        $save = $this->permissionRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Permission has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->permissionRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Permission has been deleted');
        }
        return $this->respondNotFound('Permission not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->permissionRepository, $this->permissionTransformer);

    }
}