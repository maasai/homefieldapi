<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\ExpenseRequest;
use App\HomeField\Repositories\Contracts\ExpenseInterface;
use App\HomeField\Transformers\ExpenseTransformer;
use Illuminate\Http\Request;

class ExpenseController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\ExpenseInterface
     */
    protected $expenseRepository, $expenseTransformer;

    /**
     * @param ExpenseInterface $expenseRepository
     * @param ExpenseTransformer $expenseTransformer
     */
    public function __construct(ExpenseInterface $expenseRepository, ExpenseTransformer $expenseTransformer)
    {
        $this->expenseRepository   = $expenseRepository;
        $this->expenseTransformer  = $expenseTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->expenseRepository, $this->expenseTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->expenseRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->expenseTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param ExpenseRequest $request
     * @return mixed
     */
    public function store(ExpenseRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->expenseRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $expense = $this->expenseRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $expense )
                //event(new ExpenseCreated($expense));

                return $this->respondWithSuccess('Success !! Expense has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $expense = $this->expenseRepository->getById($uuid);

        if(!$expense)
        {
            return $this->respondNotFound('Expense not found.');
        }

        return $this->respond([
            'data' => $this->expenseTransformer->transform($expense)
        ]);
    }
    /**
     * @param ExpenseRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ExpenseRequest $request, $uuid)
    {
        $save = $this->expenseRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Expense has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->expenseRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Expense has been deleted');
        }
        return $this->respondNotFound('Expense not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->expenseRepository, $this->expenseTransformer);

    }
}