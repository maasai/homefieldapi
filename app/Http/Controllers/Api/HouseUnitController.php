<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\HouseUnitRequest;
use App\HomeField\Repositories\Contracts\HouseUnitInterface;
use App\HomeField\Transformers\HouseUnitTransformer;
use Illuminate\Http\Request;

class HouseUnitController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\HouseUnitInterface
     */
    protected $houseUnitRepository, $houseUnitTransformer;

    /**
     * @param HouseUnitInterface $houseUnitRepository
     * @param HouseUnitTransformer $houseUnitTransformer
     */
    public function __construct(HouseUnitInterface $houseUnitRepository, HouseUnitTransformer $houseUnitTransformer)
    {
        $this->houseUnitRepository   = $houseUnitRepository;
        $this->houseUnitTransformer  = $houseUnitTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->houseUnitRepository, $this->houseUnitTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->houseUnitRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->houseUnitTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param HouseUnitRequest $request
     * @return mixed
     */
    public function store(HouseUnitRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->houseUnitRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{
                return $this->respondWithSuccess('Success !! HouseUnit has been created.');
        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $houseUnit = $this->houseUnitRepository->getById($uuid);

        if(!$houseUnit)
        {
            return $this->respondNotFound('HouseUnit not found.');
        }

        return $this->respond([
            'data' => $this->houseUnitTransformer->transform($houseUnit)
        ]);
    }
    /**
     * @param HouseUnitRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(HouseUnitRequest $request, $uuid)
    {
        $save = $this->houseUnitRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! HouseUnit has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->houseUnitRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! HouseUnit has been deleted');
        }
        return $this->respondNotFound('HouseUnit not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->houseUnitRepository, $this->houseUnitTransformer);

    }
}