<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\PaymentMethodRequest;
use App\HomeField\Repositories\Contracts\PaymentMethodInterface;
use App\HomeField\Transformers\PaymentMethodTransformer;
use Illuminate\Http\Request;

class PaymentMethodController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\PaymentMethodInterface
     */
    protected $paymentMethodRepository, $paymentMethodTransformer;

    /**
     * @param PaymentMethodInterface $paymentMethodRepository
     * @param PaymentMethodTransformer $paymentMethodTransformer
     */
    public function __construct(PaymentMethodInterface $paymentMethodRepository, PaymentMethodTransformer $paymentMethodTransformer)
    {
        $this->paymentMethodRepository   = $paymentMethodRepository;
        $this->paymentMethodTransformer  = $paymentMethodTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->paymentMethodRepository, $this->paymentMethodTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->paymentMethodRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->paymentMethodTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param PaymentMethodRequest $request
     * @return mixed
     */
    public function store(PaymentMethodRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->paymentMethodRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $paymentMethod = $this->paymentMethodRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $paymentMethod )
                //event(new PaymentMethodCreated($paymentMethod));

                return $this->respondWithSuccess('Success !! PaymentMethod has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $paymentMethod = $this->paymentMethodRepository->getById($uuid);

        if(!$paymentMethod)
        {
            return $this->respondNotFound('PaymentMethod not found.');
        }

        return $this->respond([
            'data' => $this->paymentMethodTransformer->transform($paymentMethod)
        ]);
    }
    /**
     * @param PaymentMethodRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(PaymentMethodRequest $request, $uuid)
    {
        $save = $this->paymentMethodRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! PaymentMethod has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->paymentMethodRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! PaymentMethod has been deleted');
        }
        return $this->respondNotFound('PaymentMethod not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->paymentMethodRepository, $this->paymentMethodTransformer);

    }
}