<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\InvoiceRequest;
use App\HomeField\Repositories\Contracts\InvoiceInterface;
use App\HomeField\Transformers\InvoiceTransformer;
use Illuminate\Http\Request;

class InvoiceController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\InvoiceInterface
     */
    protected $invoiceRepository, $invoiceTransformer;

    /**
     * @param InvoiceInterface $invoiceRepository
     * @param InvoiceTransformer $invoiceTransformer
     */
    public function __construct(InvoiceInterface $invoiceRepository, InvoiceTransformer $invoiceTransformer)
    {
        $this->invoiceRepository   = $invoiceRepository;
        $this->invoiceTransformer  = $invoiceTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->invoiceRepository, $this->invoiceTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->invoiceRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->invoiceTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param InvoiceRequest $request
     * @return mixed
     */
    public function store(InvoiceRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->invoiceRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $invoice = $this->invoiceRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $invoice )
                //event(new InvoiceCreated($invoice));

                return $this->respondWithSuccess('Success !! Invoice has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $invoice = $this->invoiceRepository->getById($uuid);

        if(!$invoice)
        {
            return $this->respondNotFound('Invoice not found.');
        }

        return $this->respond([
            'data' => $this->invoiceTransformer->transform($invoice)
        ]);
    }
    /**
     * @param InvoiceRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(InvoiceRequest $request, $uuid)
    {
        $save = $this->invoiceRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Invoice has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->invoiceRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Invoice has been deleted');
        }
        return $this->respondNotFound('Invoice not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->invoiceRepository, $this->invoiceTransformer);

    }
}