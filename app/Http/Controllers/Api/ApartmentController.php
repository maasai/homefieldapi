<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\ApartmentRequest;
use App\HomeField\Repositories\Contracts\ApartmentInterface;
use App\HomeField\Transformers\ApartmentTransformer;
use Illuminate\Http\Request;

class ApartmentController extends ApiController
{
    /**
     * @var \App\HomeField\Repositories\Contracts\ApartmentInterface
     */
    protected $apartmentRepository, $apartmentTransformer;

    /**
     * @param ApartmentInterface $apartmentRepository
     * @param ApartmentTransformer $apartmentTransformer
     */
    public function __construct(ApartmentInterface $apartmentRepository, ApartmentTransformer $apartmentTransformer)
    {
        $this->apartmentRepository   = $apartmentRepository;
        $this->apartmentTransformer  = $apartmentTransformer;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $filteredData = $this->doFilter($request, $this->apartmentRepository, $this->apartmentTransformer);

        if($filteredData){
            return $filteredData;
        }

        $load = [];

        $data = $this->apartmentRepository->getAll($load);

        return $this->respondWithPagination($data, [
            'data' => $this->apartmentTransformer->transformCollection($data->all())
        ]);
    }

    /**
     * @param ApartmentRequest $request
     * @return mixed
     */
    public function store(ApartmentRequest $request)
    {

        $data = $request->all();

        if( ! array_key_exists('password', $data )){
            $hashed_random_password = str_random(8);
            $data['password'] = $hashed_random_password;
        }

        $save = $this->apartmentRepository->create($data);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else{

            $apartment = $this->apartmentRepository->getWhere('uuid', $save['message']['uuid']);

            if( null != $apartment )
                //event(new ApartmentCreated($apartment));

            return $this->respondWithSuccess('Success !! Apartment has been created.');

        }

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function show($uuid)
    {
        $apartment = $this->apartmentRepository->getById($uuid);

        if(!$apartment)
        {
            return $this->respondNotFound('Apartment not found.');
        }

        return $this->respond([
            'data' => $this->apartmentTransformer->transform($apartment)
        ]);
    }
    /**
     * @param ApartmentRequest $request
     * @param $uuid
     * @return mixed
     */
    public function update(ApartmentRequest $request, $uuid)
    {
        $save = $this->apartmentRepository->update($request->all(), $uuid);

        if($save['error']){
            return $this->respondNotSaved($save['message']);
        }else

            return $this->respondWithSuccess('Success !! Apartment has been updated.');

    }

    /**
     * @param $uuid
     * @return mixed
     */
    public function destroy($uuid)
    {
        if($this->apartmentRepository->delete($uuid)){
            return $this->respondWithSuccess('Success !! Apartment has been deleted');
        }
        return $this->respondNotFound('Apartment not deleted');
    }

    /**
     * JSON POST data is provided
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        return $this->generalSearch($request, $this->apartmentRepository, $this->apartmentTransformer);

    }
}