<?php

namespace App\Models;

class Lease extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leases';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tenant_id',
        'apartment_id',
        'house_unit_id',
        'start_date',
        'start_date',
        'termination_notes',
        'termination_date',
        'monthly_rent',
        'rent_security_deposit',
        'rent_due_date',
        'pet_deposit',
        'electricity_deposit',
        'water_deposit',
        'total_other_deposits',
        'other_deposits_description',
        'lease_status',
        'apply_fine_on_late_payment',
        'served_by_staff_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function apartment()
    {
        return $this->belongsTo(Apartment::class, 'apartment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function houseUnit()
    {
        return $this->belongsTo(HouseUnit::class, 'house_unit_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class, 'lease_id');
    }
}