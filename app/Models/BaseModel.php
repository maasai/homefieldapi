<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class BaseModel extends Model{
    use SoftDeletes;

    public $incrementing = false;

    // protected $hidden = ['password'];

    public static function boot() {
        parent::boot();
        static::creating(function($model) {
            // Only generate the uuid if the field actually is called uuid.
            // For some system models a normal id is used (e.g. language)
            if($model->getKeyName() == 'uuid'){
                if($model->uuid != ""){

                }else
                    $model->{$model->getKeyName()} = (string)$model->generateNewId();
            }
        });
    }

    /**
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function generateNewId()
    {
        return Uuid::uuid4();
    }

    /**
     * Override save function to catch db error in case of double id's
     * @param array $options
     * @return bool|void
     */
    public function save (array $options = array())
    {
        try{
            parent::save($options);
        }catch(Exception $e){
            // check if the exception is caused by double id
            if(preg_match('/Integrity constraint violation: 1062 Duplicate entry \S+ for key \'PRIMARY\'/', $e->getMessage(), $matches)){
                $this->{$this->getKeyName()} = (string)$this->generateNewId();
                $this->save();
            }
        }
    }

    /**
     * Encrypt passwords
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

}