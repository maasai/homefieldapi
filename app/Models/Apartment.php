<?php

namespace App\Models;

class Apartment extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'apartments';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'landlord_id',
        'operations_officer_incharge_id',
        'care_taker_id',
        'apartment_name',
        'apartment_reference_number',
        'address',
        'geo',
        'smoking_allowed',
        'pets_allowed',
        'total_single_units',
        'total_bedsitters',
        'total_one_bedrooms',
        'total_two_bedrooms',
        'total_three_bedrooms',
        'total_four_bedrooms',
        'total_commercial_rooms',
        'extra_details'
    ];

    /**
     * Get the solicitor that controls this property.
     */
    public function landlord()
    {
        return $this->belongsTo(User::class, 'landlord_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function caretaker()
    {
        return $this->belongsTo(CareTaker::class, 'care_taker_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operationsOfficer()
    {
        return $this->belongsTo(User::class, 'operations_officer_incharge_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function houseUnits()
    {
        return $this->hasMany(HouseUnit::class, 'apartment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentOptions()
    {
        return $this->hasMany(ApartmentPaymentOption::class, 'apartment_id');
    }
}