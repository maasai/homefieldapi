<?php

namespace App\Models;


class Permission extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'permission_name', 'permission_display_name', 'permission_description'];

    /**
     * The users that belong to the role.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


}