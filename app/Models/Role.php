<?php


namespace App\Models;


class Role extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'role_name', 'role_display_name', 'role_description'];


    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->hasMany(User::class, 'role_id');
    }


    /**
     * The roles that belong to the user.
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'role_id', 'permission_id');
    }

}