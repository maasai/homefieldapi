<?php

namespace App\Models;

class Tenant extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tenants';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'nationality',
        'id_number',
        'passport_number',
        'postal_address',
        'marital_status',
        'number_of_intended_occupants',
        'occupant_relationship',
        'emergency_full_name',
        'emergency_postal_address',
        'emergency_phone',
        'emergency_email',
        'employment_status',
        'employment_employer',
        'employment_position_held',
        'employment_physical_address',
        'employment_phone',
        'self_employed_business',
        'self_employed_business_address',
        'self_rent_payer',
        'rent_payer',
        'rent_payer_contact',
        'date_joined',
        'commencement_of_lease',
        'kra_pin',
        'passport_photo_url'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leases()
    {
        return $this->hasMany(Lease::class, 'tenant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany(Payment::class, 'tenant_id');
    }

    /**
     * Get the solicitor that controls this property.
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}