<?php

namespace App\Models;

class HouseUnit extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'house_units';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'apartment_id',
        'house_number',
        'bedroom_count',
        'bathroom_count',
        'garage_attached',
        'area_sq_feet',
        'house_status_id',
        'commercial',
        'parking_available',
        'balcony_available',
        'floor_type',
        'notes'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function apartment()
    {
        return $this->belongsTo(Apartment::class, 'apartment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function houseStatus()
    {
        return $this->belongsTo(HouseStatus::class, 'house_status_id');
    }
}