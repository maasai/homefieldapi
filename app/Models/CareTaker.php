<?php

namespace App\Models;

class CareTaker extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'care_takers';

    /**
     * Main table primary key
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'nationality',
        'id_number',
        'kra_pin',
        'passport_number',
        'postal_address',
        'marital_status',
        'emergency_full_name',
        'emergency_postal_address',
        'emergency_phone',
        'emergency_email',
        'first_referee_full_name',
        'first_referee_phone',
        'first_referee_address',
        'first_referee_email',
        'second_referee_full_name',
        'second_referee_phone',
        'second_referee_address',
        'second_referee_email',
        'third_referee_full_name',
        'third_referee_phone',
        'third_referee_address',
        'third_referee_email',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function apartments()
    {
        return $this->hasMany(Apartment::class, 'care_taker_id');
    }

}